package media

import (
	"context"
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLocalClient(t *testing.T) {
	conf := Config{
		Local:         true,
		LocalDir:      "media",
		LocalURL:      "/media",
		RandPrefixLen: 4,
	}
	defer os.RemoveAll(conf.LocalDir)

	client, err := Open(context.TODO(), conf)
	assert.NoError(t, err)

	r := strings.NewReader("avatar data")
	link, err := client.Put(context.Background(), "avatars", "avatar.jpg", r.Size(), r)
	assert.NoError(t, err)
	assert.True(t, strings.HasPrefix(link, "/media/avatars/"))
	assert.True(t, strings.HasSuffix(link, "/avatar.jpg"))
	assert.Equal(t, 30, len(link))

	r.Reset("avatar 2 data")
	link, err = client.Put(context.Background(), "", "avatar.jpg", r.Size(), r)
	assert.NoError(t, err)
	assert.True(t, strings.HasPrefix(link, "/media/"))
	assert.True(t, strings.HasSuffix(link, "/avatar.jpg"))
	assert.Equal(t, 22, len(link))
}
