package media

import (
	"context"
	"image"
	"io"
	"mime/multipart"

	"gitlab.com/knopkalab/tracer"
)

// Client of S3 storage
//
// For opening use func Open
type Client interface {
	// Put returns link {Domain}/{Bucket}/{Category}/{RandPrefix}/{Filename}
	Put(ctx context.Context, category, filename string, size int64, body io.Reader) (link string, err error)
	// PutFile is alias Put with opening local file
	PutFile(ctx context.Context, category, filepath string) (link string, err error)
	// PutData is alias Put
	PutData(ctx context.Context, category, filename string, data []byte) (link string, err error)
	// PutImage ...
	PutImage(
		ctx context.Context, category, filename string, img image.Image, typ ImageType) (link string, err error)
	// PutMultipartThumbnail ...
	PutMultipartThumbnail(
		ctx context.Context, category string,
		file *multipart.FileHeader, typ ImageType, maxPixelSize uint) (link string, err error)
}

// Open local or s3 storage
//
// required fields:
//
//  for remote storage: AccessKey, SecretKey, Region, EndPoint, Domain, Bucket
//
//  for local storage: Local, LocalDir, LocalURL, Bucket (optional)
//
func Open(ctx context.Context, conf Config) (Client, error) {
	ctx, span := tracer.Start(ctx, "media client")

	err := conf.ValidAndRepair()
	if err != nil {
		span.Error(err).Msg("invalid config")
		span.Finish()
		return nil, err
	}

	if conf.Local {
		span.Forward(span.With().
			Str("upload_dir", conf.LocalDir),
		)
	}

	span.Debug().Msg("media client opening")

	var client minClient
	if conf.Local {
		client, err = openLocalClient(&conf)
	} else {
		client, err = openS3Client(&conf)
	}
	if err != nil {
		span.Error(err).Msg("cannot to open media client")
		span.Finish()
		return nil, err
	}

	span.Debug().Msg("media client opened")

	return newClientWrapper(ctx, client), nil
}
